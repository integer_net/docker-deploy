# PHP 8.1 ##############################################################################################################
build-php81: build-php81-fpm

build-php81-fpm:
	docker build -t integerhub/php:8.1-fpm-deploy -f images/php8.1/Dockerfile images/php8.1
.PHONY: build-php81-fpm

push-php81: build-php81
	docker login
	docker push integerhub/php:8.1-fpm-deploy
.PHONY: push-php81

# PHP 8.2 ##############################################################################################################
build-php82: build-php82-fpm

build-php82-fpm:
	docker build -t integerhub/php:8.2-fpm-deploy -f images/php8.2/Dockerfile images/php8.2
.PHONY: build-php82-fpm

push-php82: build-php82
	docker login
	docker push integerhub/php:8.2-fpm-deploy
.PHONY: push-php82

# PHP 8.3 ##############################################################################################################
build-php83: build-php83-fpm

build-php83-fpm:
	docker build -t integerhub/php:8.3-fpm-deploy -f images/php8.3/Dockerfile images/php8.3
.PHONY: build-php83-fpm

push-php83: build-php83
	docker login
	docker push integerhub/php:8.3-fpm-deploy
.PHONY: push-php83

# Cypress
# Usage: make push-cypress CYPRESS_VERSION=12.5.1
build-cypress:
	docker build --build-arg CYPRESS_VERSION="$(CYPRESS_VERSION)" --no-cache -t integerhub/cypress:$(CYPRESS_VERSION) -f images/cypress/     Dockerfile images/cypress
.PHONY: build-cypress

push-cypress: build-cypress
	docker login
	docker push integerhub/cypress:$(CYPRESS_VERSION)
.PHONY: push-cypress


# Elasticsearch
# Usage: make push-elasticsearch ELASTICSEARCH_VERSION=7.10.1
build-elasticsearch:
	docker build --build-arg ELASTICSEARCH_VERSION="$(ELASTICSEARCH_VERSION)" --no-cache -t integerhub/elasticsearch:$(ELASTICSEARCH_VERSION)-deploy -f images/elasticsearch/Dockerfile images/elasticsearch
.PHONY: build-elasticsearch

push-elasticsearch: build-elasticsearch
	docker login
	docker push integerhub/elasticsearch:$(ELASTICSEARCH_VERSION)-deploy
.PHONY: push-elasticsearch

# Opensearch
# Usage: make push-opensearch OPENSEARCH_VERSION=2.5.0
# Magento 2.4.5 - 1.2.4
# Magento 2.4.6 - 2.5.0
build-opensearch:
	docker build --build-arg OPENSEARCH_VERSION="$(OPENSEARCH_VERSION)" --no-cache -t integerhub/opensearch:$(OPENSEARCH_VERSION)-deploy -f images/opensearch/Dockerfile images/opensearch
.PHONY: build-opensearch

push-opensearch: build-opensearch
	docker login
	docker push integerhub/opensearch:$(OPENSEARCH_VERSION)-deploy
.PHONY: push-opensearch
