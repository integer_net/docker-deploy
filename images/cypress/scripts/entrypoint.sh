#!/bin/bash

cypress "$@"

# Fix permissions of output folder
if [ -d /output ]; then
  find /output -type d -exec chmod 777 {} \;
  find /output -type f -exec chmod 666 {} \;
fi
