# Docker for deployment

This docker is a short version of [Docker project](https://bitbucket.org/integer_net/docker/src/master/readme.md)
It is used by jenkins in continuous integration.

The main goal is to prepare server for test and deployment.

## Functions
- prepare server and database
- setup magento
- run tests and prepare reports
- deploy to hosting
 
## Folder structure

```
+--docker-deploy
   +--.composer             <- (ignored) contains composer cache, symlinked from local home directory (in pre-start script)
   +--.ssh                  <- (ignored) contains private SSH key provided by Jenkins (created by pre-start script if $KEYFILE variable is set) and known_hosts from deploy config
   +--bin                   <- Scripts to work with docker containers
   +--images                <- Custom Docker images
   +--nginx                 <- nginx config files
   +--scripts               <- Scripts are called at container
   |  +--magento            <- Magento related scripts
   |  |  +--test            <- Tests for Magento application 
   |  +--vue_storefront     <- Vue StoreFront related scripts
   |  |  +--test            <- Tests for Vue Storefront application
   +--.gitignore
   +--docker-compose.yml
   +--readme.md
```

## Update Docker image

This docker tool uses PHP 7.2, PHP 7.3 and PHP 7.4 Images from docker hub repository `https://hub.docker.com/repository/docker/integerhub/phpfpm-7.X-deploy`.  

Please make sure that PHP version is configured under your project `deployment/config/build.properties`:  
`PHP_VERSION=7.2`

When you want to update this image, then you should uncomment line in docker-compose.yml  
`#build: images/php${PHP_VERSION}`

and comment out line  
`image: "integerhub/phpfpm-${PHP_VERSION}-deploy"`

Make rebuild and push new image to repository.


## Usage

Before starting some should be prepared for each project.  
These files are located inside of each project. And prepared using project command `bin/docker/start`  

Examples:
- Magento 2 Application under [https://bitbucket.org/integer_net/magento2-template/src/master/](Magento 2 Template)

Every Magento installation is available under http://end2end.lh:NGINX_EXTERNAL_PORT

## Commands
| Command      | Description                                      |
|--------------|--------------------------------------------------|
| `bin/start`  | start docker                                     |
| `bin/bash`   | command line for phpfpm container                |
| `bin/dep`    | run deploy tool at phpfpm container (tty mode)   |
| `bin/npm`    | run npm installed at phpfpm container (tty mode) |
| `bin/phpfpm` | run any command at phpfpm container (tty mode)   |
| `bin/run`    | run one-off container (tty mode)                 |
| `bin/stop`   | stop docker                                      |

