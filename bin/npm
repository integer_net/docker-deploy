#!/bin/bash
[ -z "$1" ] && echo "Please specify a script to run at NODE container" && exit

DOCKER_DEPLOY_DIR=$(realpath "`dirname -- "$0"`/..")
cd "$DOCKER_DEPLOY_DIR"

# The command `bin/npm` could be called standalone.
# But it could be called right away after `bin/start`.
# Then `pre-start` is called twice and dynamic values,
# such as NGINX port is regenerated, although docker container uses old values.
# That means that after `bin/npm` the .env and .env.sh are invalid.
# Therefore we need to call `pre-start` only when `.env` has been not generated.
if [ ! -f "${DOCKER_DEPLOY_DIR}/.env" ]; then
    source bin/lib/pre-start
fi
source bin/lib/pre-docker-compose

# The default command called standalone.
DOCKER_COMMAND="run"

# Overwrite entrypoint for standalone usage.
DOCKER_ENTRYPOINT="--entrypoint=docker-entrypoint.sh"

# Check if node container is already running (via bin/start)
DOCKER_IS_RUNNING=$(${DOCKER_COMPOSE} ps --services --filter "status=running" | grep -Fxe node)
if [[ "${DOCKER_IS_RUNNING}" != "" ]]; then

    DOCKER_COMMAND="exec"

    # Default entrypoint is tail
    # defined in images/node/Dockerfile to keep it running
    DOCKER_ENTRYPOINT=""
fi

$(getDockerCompose) ${DOCKER_COMMAND} ${DOCKER_ENTRYPOINT} -T node npm "$@"
