#!/bin/bash

# This script should be called once before docker-compose up command.
# It prepares .env file and all needed folders such as .ssh and .composer

source bin/lib/pre-folders
source bin/lib/port-finder

BUILD_DIR="${REPOSITORY_ROOT_DIR}/build"
ENV_PATH="${DOCKER_DEPLOY_DIR}/.env"

# Prepare build folders outside of container
# to keep permissions.
mkdir -p "${BUILD_DIR}/artifacts"
mkdir -p "${BUILD_DIR}/reports"
touch "${BUILD_DIR}/reports/.gitignore"
mkdir -p "${BUILD_DIR}/screenshots"
mkdir -p "${BUILD_DIR}/cypress"

mkdir -p "${DOCKER_DEPLOY_DIR}/.ssh"

# known_hosts
if [[ -f "${REPOSITORY_ROOT_DIR}/deployment/config/known_hosts" ]] ; then
    cp -f "${REPOSITORY_ROOT_DIR}/deployment/config/known_hosts" "${DOCKER_DEPLOY_DIR}/.ssh/known_hosts"
else
    cp -f "${HOME}/.ssh/known_hosts" "${DOCKER_DEPLOY_DIR}/.ssh/known_hosts"
fi

# Private key id_rsa
# KEYFILE variable is defined at jenkins server
if [[ "${KEYFILE}" ]] ; then
    cp -f "${KEYFILE}" "${DOCKER_DEPLOY_DIR}/.ssh/id_rsa"
else
    cp -f "${HOME}/.ssh/id_rsa" "${DOCKER_DEPLOY_DIR}/.ssh/id_rsa"
fi

# Find composer directory with cache
COMPOSER_DIR_FOUND=0
# Composer cache directory from Jenkins
if [[ -d "${HOME}/.cache/composer/files" ]] ; then
    ln -sf "${HOME}/.cache/composer" "${DOCKER_DEPLOY_DIR}/.composer"
    COMPOSER_DIR_FOUND=1
fi
# Composer cache directory from local
if [[ -d "${HOME}/.composer/cache/files" && ${COMPOSER_DIR_FOUND} == 0 ]] ; then
    ln -sf "${HOME}/.composer" "${DOCKER_DEPLOY_DIR}/.composer"
    COMPOSER_DIR_FOUND=1
fi
# Composer cache directory is not found
if [[ ${COMPOSER_DIR_FOUND} == 0 ]] ; then
    echo "⨯ Composer cache directory is not found."
    mkdir -p "${DOCKER_DEPLOY_DIR}/.composer"
fi

## Check again new generated file
if [ ! -f "${PROPERTIES_PATH}" ]; then
  echo "⨯ ${PROPERTIES_PATH} does not exist."
  exit 1
fi


# - comment properties with dots. Properties containing a dot cannot be used as environment variables
#   (but we need them for Jenkins)
rm -f ${ENV_PATH}
sed -e 's/^[^#].*\..*=.*/#&/' "${PROPERTIES_PATH}" > "${ENV_PATH}"

# Git repository name
GIT_DIR="$REPOSITORY_ROOT_DIR"
GIT_REPOSITORY_NAME=$(basename -s .git `git -C "${GIT_DIR}" config --get remote.origin.url`)
# Git branch name
# Jenkins has prepared variable GIT_BRANCH
if [[ "${GIT_BRANCH}" ]] ; then
    GIT_BRANCH_NAME=${GIT_BRANCH}
else
    GIT_BRANCH_NAME=$(git -C "${GIT_DIR}" rev-parse --abbrev-ref HEAD)
fi

# Current WORKSPACE path
# Jenkins has prepared variable WORKSPACE
if [[ "${WORKSPACE}" ]] ; then
    WORKSPACE="${WORKSPACE}"
else
    WORKSPACE="$REPOSITORY_ROOT_DIR"
fi

# Read all variables from .env, converting into Bash syntax:
# Add quotation marks around values if they do not already exist. Properties notation as used in Jenkins and .env for
# docker compose do not use quotation marks.
# This .env.sh file is mounted as .env in the containers so that scripts can source the environment variables
# It is done here because the following variables depend on previously defined variables. At the end of the script we
# create .env.sh again with the updated values
rm -f "${ENV_PATH}.sh"
sed -e 's/^\([A-Z_]*\)=\(.*[^"]\)$/\1="\2"/' "${ENV_PATH}" > "${ENV_PATH}.sh"
# Load all variables to check empty values later
source "${ENV_PATH}.sh";

# Prepare internal variables, used by docker-compose and scripts running inside the docker containers
printf "\n# --- Environment variables created by docker-deploy/bin/lib/pre-start:\n" >> "${ENV_PATH}"

# IP address of host in docker network
# @see https://biancatamayo.me/blog/2017/11/03/docker-add-host-ip/
#HOST_IP=`ip -4 addr show scope global dev docker0 | grep inet | awk '{print \$2}' | cut -d / -f 1`
HOST_IP=`docker run --rm alpine ip route | awk 'NR==1 {print $3}'`
echo "HOST_IP=$HOST_IP" >> "$ENV_PATH"

## Name of database container used as database host
echo "MYSQL_HOST=db" >> "${ENV_PATH}"

## non-root mysql user (MySQL docker image does not allow "root" here. Use "root" explicitly with MYSQL_ROOT_PASSWORD if you need root access)
[[ ! -z "${MYSQL_USER}" ]] && sed -i 's/MYSQL_USER/#MYSQL_USER/g' "${ENV_PATH}"
echo "MYSQL_USER=magento" >> "${ENV_PATH}"

## Fallback for default host
[ -z "${DEFAULT_HOST}" ] && echo "DEFAULT_HOST=end2end.lh" >> "${ENV_PATH}"

## Force http protocol. We do not need ssl at build stage
[[ ! -z "${URL_PROTOCOL}" ]] && sed -i 's/URL_PROTOCOL/#URL_PROTOCOL/g' "${ENV_PATH}"
echo "URL_PROTOCOL=http" >> "${ENV_PATH}"

## Internal Elasticsearch port is always 9200
echo "ELASTICSEARCH_PORT=9200" >> "${ENV_PATH}"
## Internal OpenSearch port is always 9200
echo "OPENSEARCH_PORT=9200" >> "${ENV_PATH}"
## Same to opensearch and elasticsearch: always 9200
echo "SEARCH_PORT=9200" >> "${ENV_PATH}"

## ROOT DIR of Magento project
echo "WEB_ROOT_DIR=/var/www/html/${PROJECT_ROOT_DIR}" >> "${ENV_PATH}"

## Fallback for PHP Code Sniffer
[ -z "${PHPCS_DIRS}" ] && echo "PHPCS_DIRS=app/code/ app/design/" >> "${ENV_PATH}"

## Fallback for PHP Mess detector
[ -z "${PHPMD_DIRS}" ] && echo "PHPMD_DIRS=app/code/,app/design/" >> "${ENV_PATH}"

## Fallbacks for Disable/Enable test variables
[ -z "${DISABLE_TEST_STATIC}" ] && echo "DISABLE_TEST_STATIC=0" >> "${ENV_PATH}"
[ -z "${DISABLE_TEST_UNIT}" ] && echo "DISABLE_TEST_UNIT=0" >> "${ENV_PATH}"
[ -z "${DISABLE_TEST_INTEGRATION}" ] && echo "DISABLE_TEST_INTEGRATION=0" >> "${ENV_PATH}"
[ -z "${DISABLE_TEST_END2END}" ] && echo "DISABLE_TEST_END2END=1" >> "${ENV_PATH}"

## Fallback when COMPOSE_FILE is not defined in build.properties. Magento is default
[ -z "${COMPOSE_FILE}" ] && echo "COMPOSE_FILE=./docker-compose.magento.yml" >> "${ENV_PATH}"

## Fallback when SEARCH_SERVICE is not defined in build.properties. elasticsearch is default
[ -z "${SEARCH_SERVICE}" ] && echo "SEARCH_SERVICE=elasticsearch" >> "${ENV_PATH}"

## Docker compose name uses repository name and branch name to avoid conflicts
COMPOSE_PROJECT_NAME_VALUE=$(echo "deploy_${GIT_REPOSITORY_NAME}__${GIT_BRANCH_NAME/\//_}" | tr -d ' /.%' | tr '[:upper:]' '[:lower:]')
echo "COMPOSE_PROJECT_NAME=${COMPOSE_PROJECT_NAME_VALUE}" >> "${ENV_PATH}"

## Current WORKSPACE path is stored in .env
echo "WORKSPACE=${WORKSPACE}" >> "${ENV_PATH}"

# Prepare dynamic variables
printf "\n# --- Dynamic variables:\n" >> "${ENV_PATH}"

## Random DataBase Port
echo "MYSQL_EXTERNAL_PORT=$(getAvailablePortFromRange 49152 55000)" >> "${ENV_PATH}"

## Random Nginx Port
echo "NGINX_EXTERNAL_PORT=$(getAvailablePortFromRange 55001 65535)" >> "${ENV_PATH}"

## Random Elasticsearch Port
echo "ELASTICSEARCH_EXTERNAL_PORT=$(getAvailablePortFromRange 9200 9600)" >> "${ENV_PATH}"

## Random OpenSearch Port
echo "OPENSEARCH_EXTERNAL_PORT=$(getAvailablePortFromRange 9601 9999)" >> "${ENV_PATH}"

# Finally, recreate .env.sh with the new variables
sed -e 's/^\([A-Z_]*\)=\(.*[^"]\)$/\1="\2"/' "${ENV_PATH}" > "${ENV_PATH}.sh"
